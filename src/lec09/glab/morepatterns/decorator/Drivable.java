package lec09.glab.morepatterns.decorator;

public interface Drivable {

     void assemble();
}